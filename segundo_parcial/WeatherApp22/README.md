# PRÁCTICA | MI PRIMERA APLICACIÓN NATIVA

**Aplicando los conocimientos básicos obtenidos en clase vamos a desarrollar una aplicación que nos** 
**muestre el clima en nuestro celular, la cual deberá permitirnos implementar los fundamentos** 
**básicos de la navegación simple, el consumo de web services y el mapeo de estos a objetos dentro** 
**de nuestra aplicación.**

- *Miguel Eduardo Sánchez González (324795)*

## Pre-requisitos

Clonar el repositorio

Tener instalado android studio

